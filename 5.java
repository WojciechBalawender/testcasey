// Wojciech Balawender
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
public class t5 {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  @Before
  public void setUp() {
    driver = new FirefoxDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  @After
  public void tearDown() {
    driver.quit();
  }
  @Test
  public void t5() {
    driver.get("https://lamp.ii.us.edu.pl/~mtdyd/zawody/");
    driver.manage().window().setSize(new Dimension(740, 867));
    driver.findElement(By.id("inputEmail3")).click();
    driver.findElement(By.id("inputEmail3")).sendKeys("aa");
    driver.findElement(By.id("inputPassword3")).click();
    driver.findElement(By.id("inputPassword3")).sendKeys("aa");
    driver.findElement(By.id("dataU")).click();
    driver.findElement(By.id("dataU")).sendKeys("11-05-2001");
    driver.findElement(By.cssSelector(".btn")).click();
    assertThat(driver.switchTo().alert().getText(), is("Blad danych"));
    driver.close();
  }
}
